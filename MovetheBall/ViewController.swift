//
//  ViewController.swift
//  MovetheBall
//
//  Created by Innovadeaus on 12/4/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var moveableView: UIView!
    @IBOutlet weak var buttonStart: UIButton!
    
    var panGesture = UIPanGestureRecognizer()
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        startAnimating()
    }

    func startAnimating() {
        UIView.animateKeyframes(withDuration: 5, delay: 0, options: .calculationModeLinear, animations: {
            self.moveableView.center.x += self.view.bounds.width / 2
           

        }, completion: nil)
    }
    
    func initView(){
        moveableView.layer.cornerRadius = 50
        moveableView.clipsToBounds = true
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(ViewController.draggedView(_:)))
           moveableView.isUserInteractionEnabled = true
           moveableView.addGestureRecognizer(panGesture)
    }
    @objc func draggedView(_ sender:UIPanGestureRecognizer){
        self.view.bringSubviewToFront(moveableView)
        let translation = sender.translation(in: self.view)
        moveableView.center = CGPoint(x: moveableView.center.x + translation.x, y: moveableView.center.y + translation.y)
        sender.setTranslation(CGPoint.zero, in: self.view)
    }
    
    @IBAction func startStop(_ sender: Any) {
        
        if buttonStart.titleLabel?.text == "Start" {
            buttonStart.setTitle("Stop", for: .normal)
            startAnimating()
        }else{
            buttonStart.setTitle("Start", for: .normal)
            self.view.layer.removeAllAnimations()
        }
    }
}

